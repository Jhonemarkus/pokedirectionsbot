'use strict'

const Telegram = require('telegram-node-bot')
const TelegramBaseController = Telegram.TelegramBaseController
const InputFile = Telegram.InputFile;
const tg = new Telegram.Telegram('282799396:AAG9F6_jJ_1Lj-zPpJfrOyixWvusmqyjSOg')
const fs = require('fs');
const request = require('request');

class OtherwiseController extends TelegramBaseController {
    handle($){
        if($.message.entities != null){
            for(var i=0; i < $.message.entities.length; i++){
                var entity = $.message.entities[i];
                if(entity.type == 'text_link'){
                    this.sendDirections($,entity.url, $.message.text.substring(entity.offset,entity.offset+entity.length));
                }
            }
        }
    }

    sendDirections($,url,text){
        var urlParts = url.split("/");
        if(urlParts != null && urlParts.length >= 2){
            var lat = urlParts[urlParts.length-2];
            var lng = urlParts[urlParts.length-1];
            this.sendImage($,lat,lng);
        }
    }

    sendImage($,lat,lng){
        var url = "https://maps.googleapis.com/maps/api/staticmap?center="+lat+","+lng+"&markers="+
            lat+","+lng+"&size=600x800&zoom=17&key=";
        var filePath = "imgs/lat"+lat+"lng"+lng+".png"
        
        if(fileExists(filePath)){
            $.sendPhoto(InputFile.byFilePath(filePath));
        }else{
            request.head(url,function(err,res,body){
                console.log('content-type:', res.headers['content-type']);
                console.log('content-length:', res.headers['content-length']);
                request(url).pipe(fs.createWriteStream(filePath)).on('close', function(){
                    $.sendPhoto(InputFile.byFilePath(filePath));
                });
            });
        }
    }

}

function fileExists(filePath){
    try{
        return fs.statSync(filePath).isFile();
    }catch (err){
        return false;
    }
}

tg.router
    .otherwise(new OtherwiseController());